#include "Arduino.h"


/************************************StreamPrint**************************************
*http://www.utopiamechanicus.com/article/low-memory-serial-print/
*пример замены было: Serial.print("hiya!"); стало: Serialprint("hiya!");
*можно упрощать вывод в сериал было:
*Serial.print("Count: ");
*Serial.print(count);
*Serial.print(", Data: ");
*Serial.print(data);
*
*стало: Serialprint("Count %d, Data: %d",count,data);
*
*отличия от Serial.println() то что имеются знаки перевода каретки  add \n or \r
*примеры замены:
*Serialprint("Count %d, Data: %d\n",count,data);
*Streamprint () является более гибкий вариант - вы можете использовать его для других серийных изделий, 
*даже самого объекта NewSoftSerial. Вы просто передать серийный объект по вашему выбору в качестве первого
*параметра - например, здесь мы будем использовать  Serial, по сути, делает то же самое, как Serialprint ();
*
*Streamprint(Serial,"Count %d, Data: %d\n",count,data);
*
**************************************************************************************/
void StreamPrint_progmem(Print &out,PGM_P format,...)
{
  // program memory version of printf - copy of format string and result share a buffer
  // so as to avoid too much memory use
  char formatString[128], *ptr;
  strncpy_P( formatString, format, sizeof(formatString) ); // copy in from program mem
  // null terminate - leave last char since we might need it in worst case for result's \0
  formatString[ sizeof(formatString)-2 ]='\0'; 
  ptr=&formatString[ strlen(formatString)+1 ]; // our result buffer...
  va_list args;
  va_start (args,format);
  vsnprintf(ptr, sizeof(formatString)-1-strlen(formatString), formatString, args );
  va_end (args);
  formatString[ sizeof(formatString)-1 ]='\0'; 
  out.print(ptr);
}
 

//------------------------------StreamPrint------------------------------------------
