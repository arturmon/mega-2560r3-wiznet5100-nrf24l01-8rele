#include "Arduino.h"

void Check_Telnet_Connect()
{

  if (Telnetserver->available() && !connectFlag) {
    connectFlag = 1;
    client = Telnetserver->available();
    Streamprint(client,"\nWebServer Mega2560R3 Telnet Server\n\r");
    client.println(VERSION_STRING);
    client.println(COMPILE_DATE_STRING); 
    Streamprint(client,"? for help\n\r");
    printPrompt();
  }
  // check to see if text received
  if (client.connected() && client.available()) getReceivedText();

  // check to see if connection has timed out
  if(connectFlag) checkConnectionTimeout();
}



void printPrompt()
{
  timeOfLastActivity = millis();
  client.flush();
  charsReceived = 0; //count of characters received
  //client.print("\n>");
  Streamprint(client,"\r\n>");

}


void checkConnectionTimeout()
{
  if(millis() - timeOfLastActivity > allowedConnectTime) {
    Streamprint(client,"\r\n");
    Streamprint(client,"Timeout disconnect.\r\n");
    client.stop();
    connectFlag = 0;
  }
}


void getReceivedText()
{
  char c;
  int charsWaiting;

  // copy waiting characters into textBuff
  //until textBuff full, CR received, or no more characters
  charsWaiting = client.available();
  do {
    c = client.read();
    textBuff[charsReceived] = c;
    charsReceived++;
    charsWaiting--;
  }
  while(charsReceived <= textBuffSize && c != 0x0d && charsWaiting > 0);

  //if CR found go look at received text and execute command
  if(c == 0x0d) {
    parseReceivedText();
    // after completing command, print a new prompt
    printPrompt();
  }

  // if textBuff full without reaching a CR, print an error message
  if(charsReceived >= textBuffSize) {
    client.println();
    printErrorMessage(0);
    printPrompt();
  }
  // if textBuff not full and no CR, do nothing else;  
  // go back to loop until more characters are received

}  


void parseReceivedText()
{
  // look at first character and decide what to do
  switch (textBuff[0]) {
  case 'a' : 
    doAnalogCommand();        
    break;
  case 'd' : 
    doDigitalCommand();       
    break;
  case 't' : 
    doTimersCommand();   
    break;
  case 'p' : 
    setPinMode();             
    break;
  case 'i' : 
    printInfoDeviceMessage();             
    break;
  case 'l' : 
    doStructCommand();             
    break;    
  case 'c' : 
    checkCloseConnection();   
    break;
  case '?' : 
    printHelpMessage();       
    break;
  case 0x0d :                          
    break;  //ignore a carriage return
  default: 
    printErrorMessage(0);        
    break;
  }
}


void doDigitalCommand()
// if we got here, textBuff[0] = 'd'
{
  switch (textBuff[1]) {
  case 'r' : 
    readDigitalPins(); 
    break;
  case 'w' : 
    writeDigitalPin(); 
    break;
  default: 
    printErrorMessage(0); 
    break;
  }
}


void readDigitalPins()
// if we got here, textBuff[0] = 'd' and textBuff[1] = 'r'
{
  int pin;
  if (textBuff[2] == 0x0d) {
    // output the valueof each digital pin
    for (int i = 0; i < 10; i++) outputPinState(i);
  }
  else {
    pin = parseDigit(textBuff[2]);
    if(pin >=0 && pin <=9) outputPinState(pin);
    else printErrorMessage(0);
  }
}  


void outputPinState(int pin)
{
  //client.print("digital pin ");
  Streamprint(client,"digital pin ");
  // client.print(pin);
  Streamprint(client,"%d",pin);
  // client.print(" is ");
  Streamprint(client," is ");
  if (digitalRead(pin)) {
    // client.println("HIGH");
    Streamprint(client,"HIGH\r\n");
  }
  else
    //client.println("LOW");
    Streamprint(client,"LOW\r\n");
}


void writeDigitalPin()
// if we got here, textBuff[0] = 'd' and textBuff[1] = 'w'
{
  int pin = -1;
  int pinSetting = -1;
  if (textBuff[3] == '=' && textBuff[6] == 0x0d) {
    //if yes, get the pin number, setting, and set the pin
    pin = parseDigit(textBuff[2]);
    pinSetting = parsePinSetting();
    if(pin > -1 && pinSetting == 0) {
      digitalWrite(pin, LOW);
      // client.println("OK");
      Streamprint(client,"OK\r\n");
    }
    if(pin > -1 && pinSetting == 1) {
      digitalWrite(pin, HIGH);
      //client.println("OK");
      Streamprint(client,"OK\r\n");
    }
    if(pin < 0 || pinSetting < 0) printErrorMessage(0);
  }
  else printErrorMessage(0);
}


int parsePinSetting()
//look in the text buffer to find the pin setting
//return -1 if not valid
{
  int pinSetting = -1;
  if(textBuff[4] == 'l' && textBuff[5] == 'o') pinSetting = 0;
  if(textBuff[4] == 'h' && textBuff[5] == 'i') pinSetting = 1;
  return pinSetting;
}


void doAnalogCommand()
// if we got here, textBuff[0] = 'a'
{
  switch (textBuff[1]) {
  case 'r' : 
    readAnalogPins(); 
    break;
  case 'w' : 
    writeAnalogPin(); 
    break;
  default: 
    printErrorMessage(0); 
    break;
  }
}


void readAnalogPins()
// if we got here, textBuff[0] = 'a' and textBuff[1] = 'r'
// check textBuff[2] is a CR then
// output the value of each analog input pin
{
  if(textBuff[2] == 0x0d) {
    for (int i = 0; i < 6; i++) {
      //client.print("analog input ");
      Streamprint(client,"analog input ");
      //client.print(i);
      Streamprint(client,"%d",i);
      //client.print(" is ");
      //client.println(analogRead(i));
      Streamprint(client,"%d\r\n",analogRead(i));
    }
  }
  else printErrorMessage(0);
}


void writeAnalogPin()
// if we got here, textBuff[0] = 'a' and textBuff[1] = 'w'
{
  int pin = -1;
  int pwmSetting = -1;
  if (textBuff[3] == '=') {
    //if yes, get the pin number, setting, and set the pin
    pin = parseDigit(textBuff[2]);
    if(pin == 3 || pin == 5 || pin == 6 || pin == 9) {
      //   pwmSetting = parsepwmSetting();
      pwmSetting = parseSetting(4,7);//start at textBuff[4],End textBuff[7]
      if(pwmSetting >= 0 && pwmSetting <= 255) {
        analogWrite(pin,pwmSetting);
        //client.println("OK");
        Streamprint(client,"OK\r\n");
      }
      else printErrorMessage(0);
    }
    else printErrorMessage(0);
  }
  else printErrorMessage(0);
}

/*
int parsepwmSetting()
 {
 int pwmSetting = 0;
 int textPosition = 4;  //start at textBuff[4]
 int digit;
 do {
 digit = parseDigit(textBuff[textPosition]); //look for a digit in textBuff
 if (digit >= 0 && digit <=9) {              //if digit found
 pwmSetting = pwmSetting * 10 + digit;     //shift previous result and add new digit
 }
 else pwmSetting = -1;
 textPosition++;                             //go to the next position in textBuff
 }
 //if not at end of textBuff and not found a CR and not had an error, keep going
 while(textPosition < 7 && textBuff[textPosition] != 0x0d && pwmSetting > -1);
 //if value is not followed by a CR, return an error
 if(textBuff[textPosition] != 0x0d) pwmSetting = -1;  
 return pwmSetting;
 }
 */
void setPinMode()
// if we got here, textBuff[0] = 'p'
{
  int pin = -1;
  int pinModeSetting = -1;
  if (textBuff[1] == 'm' && textBuff[3] == '=' && textBuff[6] == 0x0d) {
    //if yes, get the pin number, setting, and set the pin
    pin = parseDigit(textBuff[2]);
    pinModeSetting = parseModeSetting();
    if(pin > -1 && pinModeSetting == 0) {
      pinMode(pin, OUTPUT);
      //client.println("OK");
      Streamprint(client,"OK\r\n");
    }
    if(pin > -1 && pinModeSetting == 1) {
      pinMode(pin, INPUT);
      //client.println("OK");
      Streamprint(client,"OK\r\n");
    }
    if(pin < 0 || pinModeSetting < 0) printErrorMessage(0);
  }
  else printErrorMessage(0);
}


int parseModeSetting()
//look in the text buffer to find the pin setting
//return -1 if not valid
{
  int pinSetting = -1;
  if(textBuff[4] == 'o' && textBuff[5] == 'u') pinSetting = 0;
  if(textBuff[4] == 'i' && textBuff[5] == 'n') pinSetting = 1;
  return pinSetting;
}

//-------------------------------------------------------------------------
void doTimersCommand()
{
  switch (textBuff[1]) {
  case 'r' : 
    readTimers(); 
    break;
  case 'w' : 
    writeTimers(); 
    break;
  case 's' : 
    writeStart_Stop_Timers(); 
    break;
  default: 
    printErrorMessage(0); 
    break;
  }
}

void readTimers()
// if we got here, textBuff[0] = 't' and textBuff[1] = 'r'
{
  switch (textBuff[2]){
  case '1':
    Streamprint(client,"Timer 1 - %d ms\r\n",Set_Timer_1);
    break;
  case '2':
    Streamprint(client,"Timer 2 - %d ms\r\n",Set_Timer_2);
    break;
  case '3':
    Streamprint(client,"Timer 3 - %d ms\r\n",Set_Timer_3);
    break;
  case '4':
    Streamprint(client,"Timer 4 - %d ms\r\n",Set_Timer_4);
    break;
  case '5':
    Streamprint(client,"Timer 5 - %d ms\r\n",Set_Timer_5);
    break;    
  default:
    printErrorMessage(0); 
    break;
  }    
}

void writeTimers()
// if we got here, textBuff[0] = 't' and textBuff[1] = 'w'
{
  long TimerSetting = -1;
  int timer_set = -1;
  if (textBuff[3] == '=') {
    timer_set = parseDigit(textBuff[2]);
    if(timer_set == 1 || timer_set == 2 || timer_set == 3 || timer_set == 4 || timer_set == 5) {
      TimerSetting = parseSetting(4,14);
      if(TimerSetting >= 10 && TimerSetting <= 1000000) {
        switch (timer_set){
        case 1:
          Set_Timer_1=TimerSetting;
          tmr1.set(Set_Timer_1);
          Streamprint(client,"Timer 1 - %d ms\r\n",Set_Timer_1);
          break;
        case 2:
          Set_Timer_2=TimerSetting;
          tmr2.set(Set_Timer_2);
          Streamprint(client,"Timer 2 - %d ms\r\n",Set_Timer_2);
          break;
        case 3:
          Set_Timer_3=TimerSetting;
          tmr3.set(Set_Timer_3);
          Streamprint(client,"Timer 3 - %d ms\r\n",Set_Timer_3);
          break;
        case 4:
          Set_Timer_4=TimerSetting;
          tmr4.set(Set_Timer_4);
          Streamprint(client,"Timer 4 - %d ms\r\n",Set_Timer_4);
          break;
        case 5:
          Set_Timer_5=TimerSetting;
          tmr5.set(Set_Timer_5);
          Streamprint(client,"Timer 5 - %d ms\r\n",Set_Timer_5);
          break;
        default:
          printErrorMessage(0); 
          break;    
        }

        Streamprint(client,"OK\r\n");

      }
      else printErrorMessage(1);
    }
    else printErrorMessage(0);
  }
  else printErrorMessage(0);
}


long parseSetting(int textPosition, int endTextPosition)//start at textBuff[4]
{
  long TimerSetting = 0;
  //int textPosition = 4;  //start at textBuff[4]
  int digit;
  do {
    digit = parseDigit(textBuff[textPosition]); //look for a digit in textBuff
    if (digit >= 0 && digit <=9) {              //if digit found
      TimerSetting = TimerSetting * 10 + digit;     //shift previous result and add new digit
    }
    else TimerSetting = -1;
    textPosition++;                             //go to the next position in textBuff
  }
  //if not at end of textBuff and not found a CR and not had an error, keep going
  while(textPosition < endTextPosition && textBuff[textPosition] != 0x0d && TimerSetting > -1);
  //if value is not followed by a CR, return an error
  if(textBuff[textPosition] != 0x0d) TimerSetting = -1;  
  return TimerSetting;
}


void    writeStart_Stop_Timers()
// if we got here, textBuff[0] = 't' and textBuff[1] = 's'
{
  int Timer = -1;
  int TimerSetting = -1;
  if (textBuff[3] == '=' && textBuff[6] == 0x0d) {
    //if yes, get the pin number, setting, and set the pin
    Timer = parseDigit(textBuff[2]);
    TimerSetting = parsePinSetting();
    if(Timer > -1 && TimerSetting == 0) {
      switch (Timer){
      case 1:
        tmr1.stop();
        Streamprint(client,"Timer 1 is stopped\r\n");
        break;
      case 2:
        tmr2.stop();
        Streamprint(client,"Timer 2 is stopped\r\n");
        break;
      case 3:
        tmr3.stop();
        Streamprint(client,"Timer 3 is stopped\r\n");
        break;
      case 4:
        tmr4.stop();
        Streamprint(client,"Timer 4 is stopped\r\n");
        break;
      case 5:
        tmr5.stop();
        Streamprint(client,"Timer 5 is stopped\r\n");
        break;        
      default:
        printErrorMessage(0); 
        break;    
      }   
      Streamprint(client,"OK\r\n");
    }
    if(Timer > -1 && TimerSetting == 1) {
      switch (Timer){
      case 1:
        tmr1.start();
        Streamprint(client,"Timer 1 is started\r\n");
        break;
      case 2:
        tmr2.start();
        Streamprint(client,"Timer 2 is started\r\n");
        break;
      case 3:
        tmr3.start();
        Streamprint(client,"Timer 3 is started\r\n");
        break;
      case 4:
        tmr4.start();
        Streamprint(client,"Timer 4 is started\r\n");
        break;
      case 5:
        tmr5.start();
        Streamprint(client,"Timer 5 is started\r\n");
        break;
      default:
        printErrorMessage(0); 
        break;    
      } 
      Streamprint(client,"OK\r\n");
    }
    if(Timer < 0 || TimerSetting < 0) printErrorMessage(0);
  }
  else printErrorMessage(0);
}



void doStructCommand()
// if we got here, textBuff[0] = 's'
{
  switch (textBuff[1]) {
  case 'l' : 
    list_Struct_Telnet(); 
    break;
  case 'd' : 
    Dell_ALL_Struct_Telnet(); 
    break;    
  case 's' : 
    Sort_ALL_Struct_Telnet(); 
    break;
  default: 
    printErrorMessage(0); 
    break;
  }
}


/* Отобразить на экране весь список. */
void list_Struct_Telnet(void)
{
lst.printList(); //Отображаем список на экране
}


/* Удалить весь список. */
void Dell_ALL_Struct_Telnet(void)
{
lst.delAllList(); //Отображаем список на экране
}

void Sort_ALL_Struct_Telnet(void)
{
  lst.Sort();
}
//-------------------------------------------------------------------

void printInfoDeviceMessage()
{
  Streamprint(client,"Uptime: ");
  client.println(sys.uptime());
  Streamprint(client,"RAM (byte):  %d ",sys.ramSize());
  Streamprint(client," free of  %d \r\n",sys.ramFree()); 
  sensors.requestTemperatures(); // Send the command to get temperatures
  if(sensors.getAddress(Termometers, 0))
  {
    Streamprint(client,"Temp device: ");
    client.print(sensors.getTempCByIndex(0));
    Streamprint(client,"° \r\n");
  } 
  else {
    // not found
    Streamprint(client,"Temp device: NOT FOUND");
  }


}


int parseDigit(char c)
{
  int digit = -1;
  digit = (int) c - 0x30; // subtracting 0x30 from ASCII code gives value
  if(digit < 0 || digit > 9) digit = -1;
  return digit;
}


void printErrorMessage(int error_number)
{
  switch (error_number){
  case 0:
    Streamprint(client,"Unrecognized command.  ? for help.\r\n");
    break;
  case 1:
    Streamprint(client,"Error Timer 10-1000000\r\n");
    break;
    //  case 2:
    //    Streamprint(client,"Error Tgsdgd\r\n");
    //    break;
    //  case 3:
    //    Streamprint(client,"Error dgsdsgdsgdsgdsg\r\n");
    //    break;

  }

}


void checkCloseConnection()
// if we got here, textBuff[0] = 'c', check the next two
// characters to make sure the command is valid
{
  if (textBuff[1] == 'l' && textBuff[2] == 0x0d)
    closeConnection();
  else
    printErrorMessage(0);
}


void closeConnection()
{
  //client.println("\nBye.\n");
  Streamprint(client,"\nBye.\n\r\n");
  client.stop();
  connectFlag = 0;
}


void printHelpMessage()
{
  Streamprint(client,"\nExamples of supported commands:\n\r");
  Streamprint(client,"\n\r  dr       -digital read:   returns state of digital pins 0 to 9\n\r");
  Streamprint(client,"\n\r  dr4      -digital read:   returns state of pin 4 only\n\r");
  Streamprint(client,"\n\r  ar       -analog read:    returns all analog inputs\n\r");
  Streamprint(client,"\n\r  dw0=hi   -digital write:  turn pin 0 on  valid pins are 0 to 9\n\r");    
  Streamprint(client,"\n\r  dw0=lo   -digital write:  turn pin 0 off valid pins are 0 to 9\n\r");
  Streamprint(client,"\n\r  aw3=222  -analog write:   set digital pin 3 to PWM value 222\n\r");
  Streamprint(client,"\n\r                              allowed pins are 3,5,6,9\n\r");
  Streamprint(client,"\n\r                              allowed PWM range 0 to 255\n\r");
  Streamprint(client,"\n\r  pm0=in   -pin mode:       set pin 0 to INPUT  valid pins are 0 to 9\n\r");
  Streamprint(client,"\n\r  pm0=ou   -pin mode:       set pin 0 to OUTPUT valid pins are 0 to 9\n\n\r");
  Streamprint(client,"\n\r  tr1      -timer read:     returns state of timers 1 only\r");
  Streamprint(client,"\n\r  tw3=1000 -timer write:    set timers 5 valid timer are 1 to 5\n\r");
  Streamprint(client,"\n\r                            set set the interval from 10 to 1000000\n\r");
  Streamprint(client,"\n\r  ts2=hi   -timer start:    set start timer 2\n\r");
  Streamprint(client,"\n\r  ts1=lo   -timer stop:     set stoped timer 1\n\r");
  Streamprint(client,"\n\r                            timers 1,2,3,4,5\n\r");
  Streamprint(client,"\n\r  ll       -struct list:    return list Sensors struct\n\r");
  Streamprint(client,"\n\r  ld       -del all list:   delete all Sensors struct\n\r");
  Streamprint(client,"\n\r  ls       -struct sort:    sort all Sensors struct\n\r");  
  
  Streamprint(client,"\n\r  i        -Info:           uptime, ram, free ram, temp device\n\r");   
  Streamprint(client,"\n\r  cl       -close connection\n\r");
  Streamprint(client,"\n\r  ?        -print this help message\n\r");
}












