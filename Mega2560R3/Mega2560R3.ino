

/**
 * Контроллер-исполнительное устройство 
 * Platform: Arduino UNO R3 + EthernetShield W5100
 * IDE: Arduino 1.5.2
 *
 * обращение по http://xx.xx.xx.xx/ выдаст справочную информацию по этому устройству (нужно для того, чтобы когда обращаешься
 * по IP к устройству понять что это за контроллер и пр.)
 *
 * /state - состояние всез портов
 * /command - выполнение команды
 *         команды можно вызывать серией в 1 запросе. Например http://xx.xx.xx.xx/command?3=CLICK&4=CLICK&5=ON&6=OFF
 *         только длинна строки запроса не должна привышать maxLength
 * /getdev - получить список всех устройст на 1-wire
 *         формат вывода: 
 *                T<номер устройства на шине>:<HEX адрес устройства>:<текущая температура в градусах цельсия>;[...]
 *                (пример T0:1060CF59010800E3:24.06;T1:109ABE59010800FE:24.56;)
 *
 * Interrupt 4 (порт 19)
 **/

#include <math.h>

#include "DHT.h"

#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <Time.h> 

#include <SdFat.h>
#include "IniFile.h"
#define BUFFER_SIZE 512 //512

#include <Arduino.h>
#include "WebServer.h" // Webduino (https://github.com/sirleech/Webduino)
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include <Wire.h>

#include <longtime.h>

#include <digitalWriteFast.h>

#include <aJSON.h>

#include <avr/pgmspace.h>
#include <TextFinder.h>

#include "system.h"
System sys;

#include <avr/sleep.h>
#include <avr/power.h>

//***********************************Define Mega && iBoard********************************
//#define USED_iBoardRF24;  // Если используем Iboard Раскоментировать
#define USED_MEGA2560R3; //Если используем Iboard то коментируем


#ifdef USED_iBoardRF24
#include <iBoardRF24.h>
#include <iBoardRF24Network.h>
#include <digitalWriteFast.h>
#endif

#ifdef USED_MEGA2560R3
#include "nRF24L01.h"
#include "RF24.h"
#include <RF24Network.h>
#endif

//***********************************Define Mega && iBoard********************************

#include "printf.h"


//--------------------
#include "A09_setup_SD.h"
#include "A15_Utils_function.h"
#include "A50_HTML_setup_WIFI.h"
#include "A51_HTML_setup_Pin.h"
#include "A52_HTML_setup_Rele.h"
#include "A53_HTML_setup_NET.h"
#include "A54_HTML_info.h"
#include "A55_HTML_get_1wire_device.h"
#include "A60_HTML_system_util.h"
#include "A80_HTML_Command_execution.h"
#include "A100_HTML_index.h"
#include "A20_Logic_of_the_Structure.h"
//#include "A25_Class_Buttons"



//-----------------------------UDP Time------------------------------
    IPAddress ip_ntp;
//IPAddress timeServer(132, 163, 4, 101); // time-a.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 102); // time-b.timefreq.bldrdoc.gov
// IPAddress timeServer(132, 163, 4, 103); // time-c.timefreq.bldrdoc.gov


/* Set this to the offset (in seconds) to your local time
   This example is GMT - 6 
const long timeZoneOffset = -21600L;  */

int timeZone;
//const int timeZone = 1;     // Central European Time
//const int timeZone = -5;  // Eastern Standard Time (USA)
//const int timeZone = -4;  // Eastern Daylight Time (USA)
//const int timeZone = -8;  // Pacific Standard Time (USA)
//const int timeZone = -7;  // Pacific Daylight Time (USA)


EthernetUDP Udp;
unsigned int localPort = 8888;  // local port to listen for UDP packets

/*-------- NTP code ----------*/

const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets


  time_t prevDisplay = 0; // when the digital clock was displayed
//-----------------------------UDP Time------------------------------

//----------------------------Define_Modules-------------------------

//#define USED_GSM;
#ifdef USED_GSM
#endif

//----------------------------Define_Modules-------------------------
#define DEDUG;
//---------------------------Include_GSM-----------------------------
#ifdef USED_GSM
#include "SIM900.h"
#include <SoftwareSerial.h>
#include "sms.h"
SMSGSM sms;

int numdata;
boolean started=false;
char smsbuffer[160];
char n[20];
/*
 Note:
 Not all pins on the Mega and Mega 2560 support change interrupts, 
 so only the following can be used for RX: 
 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69
 
 Not all pins on the Leonardo support change interrupts, 
 so only the following can be used for RX: 
 8, 9, 10, 11, 14 (MISO), 15 (SCK), 16 (MOSI).
 */
//SoftwareSerial Serial1(10, 11); //RX,TX
/*
или хардварный сериал для Mega
 Дополнительно, только для Arduino Mega:
 
 Serial1.begin(speed)
 Serial2.begin(speed)
 Serial3.begin(speed)
 
 Последовательная шина: 0 (RX) и 1 (TX); Последовательная шина 1: 19 (RX) и 18 (TX); 
 Последовательная шина 2: 17 (RX) и 16 (TX); Последовательная шина 3: 15 (RX) и 14 (TX). 
 Выводы используются для получения (RX) и передачи (TX) данных TTL. 
 Выводы 0 и 1 подключены к соответствующим выводам микросхемы последовательной шины ATmega8U2.
 */
#endif
//---------------------------Include_GSM-----------------------------




void(* resetFunc) (void) = 0; // Reset MC function

//--------------------------Include_LiquidCrystal_I2C----------------------------------------
#include <LiquidCrystal_I2C.h>
//--------------------------Include_LiquidCrystal_I2C----------------------------------------

//--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------
// set the LCD address to 0x27 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
//http://hmario.home.xs4all.nl/arduino/LiquidCrystal_I2C/LiquidCrystal_I2C.zip
uint8_t Adress_LCD_I2C = 0x27;
LiquidCrystal_I2C lcd(Adress_LCD_I2C, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Адресация пинов у переходника i2c
//LiquidCrystal_I2C lcd(Adress_LCD_I2C, 4, 5, 6, 0, 1, 2, 3, 7, NEGATIVE);  // Другой вид переходника i2c
long count_Print_LCD;
uint8_t temp_cel[8] =
{
  B00111,
  B00101,
  B00111,
  B00000,
  B00000,
  B00000,
  B00000
};
byte Rele_is_on[8] = {
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111
}; 
byte Rele_is_off[8] = {
  B11111,
  B10001,
  B10001,
  B10001,
  B10001,
  B10001,
  B11111
}; 

const uint8_t Type_Screen_LCD = 2; //1  скролинг, 2 перелистывание

uint8_t count_Screen_LCD;
//--------------------------Inicialise_LiquidCrystal_I2C-------------------------------------

//--------------------------Version----------------------------------------------------------
#define VERSION_STRING "0.0.22(DS,Rele,AnIn,DigIn,LanSetup,Wifi_client_read,Wifi_client_write,Telnet_Server,SDfat,JSON,NTP_Client)"
#define COMPILE_DATE_STRING __DATE__
//--------------------------Version----------------------------------------------------------

//-----------------------------------Counter-------------------------------------------------
boolean Impuls, lastImpuls;
int CounterState;

long lastDebounceTime = 0;
long debounceDelay = 50;
//-----------------------------------Counter-------------------------------------------------

//-----------------------------------Устанавливаем таймеры-----------------------------------
long Set_Timer_1=50;
long Set_Timer_2=2000;
long Set_Timer_3=5000;
long Set_Timer_4=15000;
long Set_Timer_5=10800000; //каждые 3 часа
CLongTimer tmr1(Set_Timer_1);//через какое время будет проиведен следующий опрос elapsed1
CLongTimer tmr2(Set_Timer_2);//через какое время будет проиведен следующий опрос elapsed2
CLongTimer tmr3(Set_Timer_3);//через какое время будет проиведен следующий опрос elapsed3 !!!!время отправки данных температуры на сервер
CLongTimer tmr4(Set_Timer_4);
CLongTimer tmr5(Set_Timer_5);

#define textBuffSize 12 //length of longest command string plus two spaces for CR + LF

char textBuff[textBuffSize]; //someplace to put received text
int charsReceived = 0;

boolean connectFlag = 0; //we'll use a flag separate from client.connected
//so we can recognize when a new connection has been created
unsigned long timeOfLastActivity; //time in milliseconds of last activity
unsigned long allowedConnectTime = 300000; //five minutes

//-----------------------------------Уствнвыливаем таймеры-----------------------------------

//------------------------------StreamPrint------------------------------------------
#define Serialprint(format, ...) StreamPrint_progmem(Serial,PSTR(format),##__VA_ARGS__)
#define Streamprint(stream,format, ...) StreamPrint_progmem(stream,PSTR(format),##__VA_ARGS__)
//------------------------------StreamPrint------------------------------------------

/************************************Rele***************************************************/
#define ON 0
#define OFF 1




const byte Number_of_Sensors = 38;
byte Sensor_ID[Number_of_Sensors] ={
  19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5   };//16 rele + 14 analog_in + 8 digital_in
byte Sensor_PIN[Number_of_Sensors] ={
  30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,97,96,95,94,93,92,83,83,84,85,86,87,88,89,22,23,24,25,26,27,28,29    };



/************************************Rele***************************************************/
/*
//--------------------------PIR_Sensor-------------------------------------------------------
 int pirState_A[Analog_IN];
 int val_pir_A[Analog_IN];
 int pirState_D[Digital_IN];
 int val_pir_D[Digital_IN];
 //--------------------------PIR_Sensor-------------------------------------------------------
 */
// Pin controller for connection data pin DS18S20
#define ONE_WIRE_BUS 9 // Digital D9 pin Arduino (куда подключен вход для датчиков DS18X20)

//--------------------------KEY--------------------------------------------------------------


//--------------------------Pre_define_DHT---------------------------------------------------
DHT dht;
//--------------------------Pre_define_DHT---------------------------------------------------

//--------------------------Pre_define_Switch_SD_to_Ethernet-----------------------------------

// На Ethernet Shield, CS является вывод 4. Обратите внимание, что даже если это не
// Используется в качестве штифта CS, аппаратные CS контактный (10 на большинстве плат Arduino,
// 53 на Mega) должны быть оставлены в качестве выхода или библиотеке SD
// Функции не будут работать.
int selectEthernet = 53;          // выбор ведомого на шилде - Ethernet
int selectSd = 4;                 // выбор ведомого на шилде - SD

// включим Ethernet
#define SWITCH_TO_W5100 digitalWrite(selectSd,HIGH); digitalWrite(selectEthernet,LOW);
// включим SD
#define SWITCH_TO_SD digitalWrite(selectEthernet,HIGH); digitalWrite(selectSd,LOW);
// выключим и SD и Ethernet
#define ALL_OFF digitalWrite(selectEthernet,HIGH); digitalWrite(selectSd,HIGH);

SdFat sd;
Sd2Card card;//--------------SD
SdVolume volume;//--------------SD
SdFile root;//--------------SD
SdFile myFile;//--------------SD

//--------------------------Pre_define_Switch_SD_to_Ethernet-----------------------------------



EthernetClient client;
char buf[100];
//--------------------------Pre_define_Ethernet----------------------------------------------

//--------------------------Pre_define_Fast_ADC----------------------------------------------
#define FASTADC 1

// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif
//--------------------------Pre_define_Fast_ADC----------------------------------------------

//--------------------------Pre_define_DS*---------------------------------------------------
#define TEMPERATURE_PRECISION 9
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress Termometers;
float tempC; 
//--------------------------Pre_define_DS*---------------------------------------------------

//--------------------------Pre_define_command_Click-----------------------------------------
#define delayClick 1000 // задержка при обычном CLICK
#define delayLClick 3000 // задержка при длинном LCLICK
//--------------------------Pre_define_command_Click-----------------------------------------

//--------------------------Pre_define_parce_command-----------------------------------------
#define NAMELEN 100
#define VALUELEN 100

//--------------------------Pre_define_parce_command-----------------------------------------

//--------------------------Pre_define_Максимальная_Длинна_Команд_и_Параметров---------------


/* Store all string in the FLASH storage to free SRAM.
 The P() is a function from Webduino.
 */
P(Page_start) = "<html><head><title>Web EEPROM Setup</title></head><body>\n";
P(Page_end) = "</body></html>";

P(Http400) = "HTTP 400 - BAD REQUEST";



//----------------------------------------------------define_old_Web_interface------------------------------

String url = String(60);
int maxLength=50; // Максимальная длинна строки запроса

//--------------------------Pre_define_Размер_таблицы_команд---------------------------------
#define MAX_COMMAND_LEN             (13)
#define MAX_PARAMETER_LEN           (13)
#define COMMAND_TABLE_SIZE          (13)
//--------------------------Pre_define_Максимальная_Длинна_Команд_и_Параметров---------------

char gCommandBuffer[MAX_COMMAND_LEN + 1];
char gParamBuffer[MAX_PARAMETER_LEN + 1];
long gParamValue;

typedef struct {
  char const    *name;
  void          (*function)(WebServer &server);
} 
command_t;

command_t const gCommandTable[COMMAND_TABLE_SIZE] = {
  //  {"LED",     commandsLed, },
  {
    "HELP",     commandsHelp,                     }
  , // Выводит список комманд (вызов http://xx.xx.xx.xx/command?8=HELP )
  {
    "ON",     commandsOn,                     }
  , // Устанавливает "1" на заданном цифровом порту (вызов http://xx.xx.xx.xx/command?8=ON )
  {
    "OFF",     commandsOff,                     }
  , // Устанавливает "0" на заданном цифровом порту (вызов http://xx.xx.xx.xx/command?8=OFF )
  {
    "STATUS",     commandsStatus,                     }
  , // Получить состояние цифрового порта (1 или 0) (вызов http://xx.xx.xx.xx/command?8=STATUS )
  // если вместо номера порта передать ALL (вызов http://xx.xx.xx.xx/command?ALL=STATUS ), то получим состояние всех портов (Пример вывода P3=0;P4=0;P5=0;P6=0;P7=0;P8=1;P9=1;)
  {
    "CLICK",     commandsClick,                     }
  , // Кратковременная "1" на порту 1сек (время настраивается) (вызов http://xx.xx.xx.xx/command?8=CLICK ) , команды можно вызывать серией в 1 запросе. Например http://xx.xx.xx.xx/command?3=CLICK&4=CLICK&5=ON&6=OFF
  {
    "LCLICK",     commandsLClick,                     }
  , // Кратковременная "1" на порту 3сек (время настраивается) (вызов http://xx.xx.xx.xx/command?8=LCLICK )
  {
    "READ", commands_Read_Sensor,                     }
  ,          // Получить состояние аналогово порта (вызов http://xx.xx.xx.xx/command?1=AN )
  {
    "AN", commandsAn,                     }
  ,          // Получить состояние аналогово порта (вызов http://xx.xx.xx.xx/command?1=AN )
  {
    "DIG", commandsDig,                     }
  ,          // Получить состояние аналогово порта (вызов http://xx.xx.xx.xx/command?1=DIG )  
  {
    "DHT", commandsDht,                     }
  ,        // Получить состояние датчика DHT (вызов http://xx.xx.xx.xx/command?2=DHT )
  {
    "SWITCH", commandsSwitch,                     }
  , // Переключить состояние цифрового порта на противоположное (вызов http://xx.xx.xx.xx/command?8=SWITCH )

  {
    NULL,      NULL                     }
};
//----------------------------------------------------define_old_Web_interface------------------------------




//--------------------------Pre_define_nRF24L01----------------------------------------------
// Set up nRF24L01 radio on SPI bus plus pins 9 & 10 (40,53)
//
// Hardware configuration
//

struct payload_t
{
  byte type;          //<< Тип датчика
  byte number;        //<< Номер датчика
  unsigned int value;                //<< Значения датчика
}
F_tt;


//***********************************Define Mega && iBoard********************************

#ifdef USED_iBoardRF24
iBoardRF24 radio(12,11,8,7,9,PE7); 
iBoardRF24Network network(radio);
#endif

#ifdef USED_MEGA2560R3
RF24 radio(40,53);//rf_ce,rf_csn
RF24Network network(radio);
#endif

//***********************************Define Mega && iBoard********************************


// Наш адрес узла
uint16_t this_node;

// Массив узлов известных нам
const short max_active_nodes = 10;
uint16_t active_nodes[max_active_nodes];
short num_active_nodes = 0;
short next_ping_node_index = 0;


// Прототипы для функций для отправки и обработки сообщений
bool send_M(uint16_t to);
bool send_N(uint16_t to);
//bool send_C(uint16_t to);
void handle_M(RF24NetworkHeader& header);
void handle_N(RF24NetworkHeader& header);
//void handle_I(RF24NetworkHeader& header);
void add_node(uint16_t node);

//--------------------------Pre_define_nRF24L01----------------------------------------------*/




//--------------------------Ссылки на Web и Telnet серверы------------------------------------*/
/* This creates an pointer to instance of the webserver. */

WebServer * webserver;
WebServer * Telnetserver;


float Device_temp_HTML;




#pragma pack(push, 1)
struct Element		
{
public:
  unsigned int modul_ID;                                            //клиентов может быть теоретически
  byte sensor_numder;                                               //сенсоров подключенных к одному клиенту
  byte sensor_type;                                                 //ID сенсора
  int sensor_value;                                                // значение типа переменной
  byte hour_struct;
  byte minute_struct;
  byte second_struct;
  byte day_struct;
  byte month_struct;
  int year_struct;
  
  Element *next;                                                   // указатели на следующий
  Element *pred;                                                   //и на предыдущий элементы списка
};
#pragma pack(pop) 


class List
{
public:
  Element *pHead;                                                  // указатель на первый элемент списка
  Element *pPrev;                                                  // указатель на последний элемент списка
  int countElem;                                                   // количество элементов в списке

  List();		                                           //конструктор
  ~List();		                                           //деструктор

  void addToList(unsigned int modul_ID,byte sensor_numder,byte sensor_type,int sensor_value,byte hour_struct,byte minute_struct,byte second_struct,byte day_struct,byte month_struct,int year_struct);	//функция добавки элемента (слова) в список
  void printList();			                           //функция вывода на экран всех элементов списка
  void delAllList();			                           //функция удаления всех элементов списка
  void delElem(int numb);		                           //удаление одного элемента
  void Sort();
  Element * Search(unsigned int modul_ID,byte sensor_numder,byte sensor_type, byte type_search);
  int Edit(unsigned int modul_ID_3, byte sensor_numde_3, byte sensor_type_3,int state, byte type_Edit,byte hour_struct,byte minute_struct,byte second_struct,byte day_struct,byte month_struct,int year_struct);
};

List::List()				
{
  pHead = NULL;
  pPrev = NULL;
  countElem = 0;
}

List::~List()			
{
  delAllList();
}



void List::addToList(unsigned int modul_ID,byte sensor_numder,byte sensor_type,int sensor_value,byte hour_struct,byte minute_struct,byte second_struct,byte day_struct,byte month_struct,int year_struct)	//реализация функции добавления
{
  Element *temp = new Element;		                           //создается временный элемент
  if(pHead == NULL)			                           //если это первый элемент, то
  {
    temp->pred = NULL;		                                   //обнуляем указатель на предшествующий 
    //элемент т.к. его нет
    temp->next = NULL;		                                   //то же самое с последующим элементом
    pHead = temp;		                                   //"голова" указывает на созданный элемент
  }
  else					                           //если не первый, то
  pPrev->next = temp;		                                   //предыдущий указывает на него

  temp->modul_ID=modul_ID;                                         //Записываем значение в структуру
  temp->sensor_numder=sensor_numder;                               //Записываем значение в структуру
  temp->sensor_type=sensor_type;                                   //Записываем значение в структуру
  temp->sensor_value=sensor_value;                                 //Записываем значение в структуру    

  temp->hour_struct=hour_struct;
  temp->minute_struct=minute_struct;
  temp->second_struct=second_struct;
  temp->day_struct=day_struct;
  temp->month_struct=month_struct;
  temp->year_struct=year_struct;

  temp->next = NULL;	                                           //последующего элемента нет (добавляем же в конец)
  temp->pred = pPrev;                                              //указываем на предыдущий элемент, на который «нацелен» pPrev
  pPrev = temp;	                                                   //а теперь хвостовой элемент указывает на последний (добавленный)
  countElem++;	                                                   //увеличиваем счетчик элементов в списке
}



void List::printList()				                   //реализация функции вывода на экран списка
{
  Element *pTemp = pHead;		                           //создается временный элемент

  if (pHead == NULL)			                           //если список пуст, то 
  {
    Streamprint(Serial,"Spisok pust\n");	                           //выводим соответствующее сообщение
  }
  else					                           //если все такие он не пуст, то
  {
    Streamprint(Serial,"Spisok ->: \n");		

    while(pTemp != NULL)	                                   //пока врем. элем. не будет указывать на хвост
    {
      //выводим данные элемента

      //Streamprint(Serial,"%d %d %d %d \n",pTemp->modul_ID,pTemp->sensor_numder,pTemp->sensor_type,pTemp->sensor_value);
      //Streamprint(client,"%d %d %d %d %d %d %d %d\n\r",pTemp->modul_ID,pTemp->sensor_numder,pTemp->sensor_type,pTemp->sensor_value,pTemp->hour_struct,pTemp->minute_struct,pTemp->second_struct,pTemp->day_struct,pTemp->month_struct,pTemp->year_struct);
      Streamprint(client,"node-%d(%d) ID-%d Val-%d %d:%d:%d-%d.%d.%d\n\r",pTemp->modul_ID,pTemp->sensor_numder,pTemp->sensor_type,pTemp->sensor_value,pTemp->hour_struct,pTemp->minute_struct,pTemp->second_struct,pTemp->day_struct,pTemp->month_struct,pTemp->year_struct);
      pTemp = pTemp->next;	                                   //и переходим на следующий за ним элем.
    }
    Serial.println();
  }
}


void List::delElem(int numb)			                   //функция удаления элемента
{
  Element *pTemp = pHead;		                           //создаем временный элемент


  if ((numb>countElem) || (numb<1))	                           //если указанный элемент не существует, то
    //выводим предупреждение на экран
    Streamprint(Serial,"Takogo elementa net\n");
  else
  {
    for(int i=1; i!=numb; i++)	                                   //иначе, переходим до этого элемента
    {
      pTemp = pTemp->next;
    }

    if (pTemp->pred == NULL) 		                           //если удаляем первый элемент
    {
      if (countElem == 1) 		                           //если этот элемент единственный
      {
        pHead = NULL;
        pPrev = NULL;
      }
      else 				                           //если он первый, но не единственный
      {
        pTemp->next->pred = NULL;
        pHead = pTemp->next;
      }

      delete pTemp;
      countElem--;

      Streamprint(Serial,"Element %d udalen\n",numb);

      return;
    }

    if (pTemp->next == NULL)		                            //если удаляем последний элемент, то
    {
      pTemp->pred->next = NULL;	                                    //предыдущий элемент указывает 
      //на NULL
      pPrev = pTemp->pred;		                            //указатель на последний элемент 
      //указывает на предпоследний

      delete pTemp;
      countElem--;

      Streamprint(Serial,"Element %d udalen\n",numb);

      return;
    }		

    //если элемент находится в центре списка
    if (pTemp->next != NULL && pTemp->pred != NULL) 
    {
      pTemp->pred->next = pTemp->next;                              //предыдущий элемент указывает 
      //на следующий
      pTemp->next->pred = pTemp->pred;                              //следующий указывает на 
      //предыдущий
      delete pTemp;
      countElem--;

      Streamprint(Serial,"Element %d udalen\n",numb);

      return;
    }
  }
};




void List::delAllList()		                                     //реализация удаления всех элементов списка
{
  while(pHead != NULL)		                                     //пока не указываем на хвост
  {
    Element *pTemp = pHead;	                                     //создаем временный элемент
    pHead = pHead->next;		                             //присваиваем ему указатель на следующий
    delete pTemp;			                             // и удаляем его
  }

  pHead = NULL;
  pPrev = NULL;
  countElem = 0;
}


void List::Sort()                                                    //пузырьковая сортировка по 1=modul_ID
{

  Element * list = pHead;                                            // связанный список
  Element * Element, * Element2;
  Streamprint(Serial,"--------------------Sorting--------------------\n");
  for( Element = list; Element; Element = Element->next )
    for( Element2 = list; Element2; Element2 = Element2->next )

      if( Element->modul_ID < Element2->modul_ID){                  // если число из node меньше числа из node2 то переставляем их

        unsigned int i = Element->modul_ID;
        Element->modul_ID = Element2->modul_ID;
        Element2->modul_ID = i;

        byte u = Element->sensor_numder;        
        Element->sensor_numder = Element2->sensor_numder;
        Element2->sensor_numder = u;       

        byte y = Element->sensor_type;        
        Element->sensor_type = Element2->sensor_type;
        Element2->sensor_type = y;   

        int t = Element->sensor_value;        
        Element->sensor_value = Element2->sensor_value;
        Element2->sensor_value = t;     
   
        int t1 = Element->hour_struct;        
        Element->hour_struct = Element2->hour_struct;
        Element2->hour_struct = t1;   

        int t2 = Element->minute_struct;        
        Element->minute_struct = Element2->minute_struct;
        Element2->minute_struct = t2; 

        int t3 = Element->second_struct;        
        Element->second_struct = Element2->second_struct;
        Element2->second_struct = t3; 

        int t4 = Element->day_struct;        
        Element->day_struct = Element2->day_struct;
        Element2->day_struct = t4; 

        int t5 = Element->month_struct;        
        Element->month_struct = Element2->month_struct;
        Element2->month_struct = t5;         
        
        int t6 = Element->year_struct;        
        Element->year_struct = Element2->year_struct;
        Element2->year_struct = t6;         
      }

}



// Поиск компонента в списке по имени
Element * List::Search(unsigned int modul_ID_2,byte sensor_numder_2,byte sensor_type_2, byte type_search)
{
  Element *pTemp = pHead;	                                     //создаем временный элемент
  if (pHead == NULL)
  {
    Streamprint(Serial,"Spisok pust\n");	                     //выводим соответствующее сообщение
    return 0;
  }
  else					                             //если все такие он не пуст, то
  {
    switch (type_search) {
    case 1:
      while(pTemp != NULL)		                             //пока не указываем на хвост
      {
        if (pTemp->modul_ID  == modul_ID_2 && pTemp->sensor_numder == sensor_numder_2 && pTemp->sensor_type == sensor_type_2)
          return pTemp;
        pTemp = pTemp->next;		                             //присваиваем ему указатель на следующий
      }
    case 2:
      while(pTemp != NULL)		                             //пока не указываем на хвост
      {
        if (pTemp->modul_ID  == modul_ID_2 && pTemp->sensor_numder == sensor_numder_2)
          return pTemp;
        pTemp = pTemp->next;		                             //присваиваем ему указатель на следующий
      }
    case 3:
      while(pTemp != NULL)		                             //пока не указываем на хвост
      {
        if (pTemp->modul_ID  == modul_ID_2 && pTemp->sensor_type == sensor_type_2)
          return pTemp;
        pTemp = pTemp->next;		                             //присваиваем ему указатель на следующий
      }      
    default:    
      return 0;
    }
  }


}



// Изменение значения компонента
int List::Edit(unsigned int modul_ID_3, byte sensor_numde_3, byte sensor_type_3,int state, byte type_Edit,byte hour_struct,byte minute_struct,byte second_struct,byte day_struct,byte month_struct,int year_struct)
{
  Element *pTemp = pHead;  	                                     //создаем временный элемент
  switch (type_Edit) {
  case 1:
    {
      pTemp = Search(modul_ID_3, sensor_numde_3,sensor_type_3,1);
      if (pTemp != 0){
        pTemp -> sensor_value = state;
        pTemp->hour_struct = hour_struct;
        pTemp->minute_struct = minute_struct;
        pTemp->second_struct = second_struct;
        pTemp->day_struct = day_struct;
        pTemp->month_struct = month_struct;
        pTemp->year_struct = year_struct;
        return 1; 
      }
      //else  Streamprint(Serial,"Error Nothing modul_ID || sensor_numde\n");
      else  return 0;
    }

  case 2:
    {
      pTemp = Search(modul_ID_3, sensor_numde_3,0,2);
      if (pTemp != 0){
        pTemp -> sensor_type = state;
        pTemp->hour_struct = hour_struct;
        pTemp->minute_struct = minute_struct;
        pTemp->second_struct = second_struct;
        pTemp->day_struct = day_struct;
        pTemp->month_struct = month_struct;
        pTemp->year_struct = year_struct;
        return 1; 
      }
      //else  Streamprint(Serial,"Error Nothing modul_ID || sensor_numde\n");
      else  return 0;        
    }
  default:    
    return 0;
  }
} 


List lst;



