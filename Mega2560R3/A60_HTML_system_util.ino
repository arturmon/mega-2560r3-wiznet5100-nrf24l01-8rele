#include "Arduino.h"

/**********************************************************************************************************************/

void ErrorMessage(WebServer &server) {
  server.print("Ошибка: Этот порт не настроен ");
}

/**********************************************************************************************************************
 * Разбор запроса
 **/
void parsedRequest(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  URLPARAM_RESULT rc;
  char name[NAMELEN];
  int  name_len;
  char value[VALUELEN];
  int value_len;

  server.httpSuccess();  // this line sends the standard "we're all OK" headers back to the browser

  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */
  if (type == WebServer::HEAD)
    return;

  if (strlen(url_tail))
  {
    while (strlen(url_tail)) // Разбор URI на составные части (выборка параметров)
    {
      rc = server.nextURLparam(&url_tail, name, NAMELEN, value, VALUELEN);
      if (rc == URLPARAM_EOS) {
        //      server.printP(Params_end);
      }
      else // Получили параметр (name) и его значение (value)
      {
        // Выполняем команды
        strcpy (gCommandBuffer, value); // параметры (значение)
        strcpy (gParamBuffer, name); // команда
        cliProcessCommand(server);
      }
    }
  }
 /*     
   if (type == WebServer::POST)
   {
   server.printP(Post_params_begin);
   while (server.readPOSTparam(name, NAMELEN, value, VALUELEN))
   {
   server.print(name);
   server.printP(Parsed_item_separator);
   server.print(value);
   server.printP(Tail_end);
   }
   }
  */ 

}
void stateRequest(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  server.httpSuccess();  // this line sends the standard "we're all OK" headers back to the browser

  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */
  if (type == WebServer::HEAD)
    return;

  strcpy (gParamBuffer, "ALL");
  commandsStatus(server);
}

/**
 * errorHTML() function
 * This function is called whenever a non extisting page is called.
 * It sends a HTTP 400 Bad Request header and the same as text.
 */
void errorHTML(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  /* this line sends the standard "HTTP 400 Bad Request" headers back to the
   browser */
  server.httpFail();

  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */
  if (type == WebServer::HEAD)
    return;

  server.printP(Http400);

  server.printP(Page_end);
}


/**
 * errorHTML() function
 * This function is called whenever a non extisting page is called.
 * It sends a HTTP 400 Bad Request header and the same as text.
 */

/**********************************************************************************************************************
 *
 * Function:    cliProcessCommand
 *
 * Description: Look up the command in the command table. If the
 *              command is found, call the command's function. If the
 *              command is not found, output an error message.
 *
 * Notes:       
 *
 * Returns:     None.
 *
 **********************************************************************/
// несколько комманд ?1=AN&2=AN
void cliProcessCommand(WebServer &server)
{
  int bCommandFound = false;
  int idx;

  gParamValue = strtol(gParamBuffer, NULL, 0);  // Convert the parameter to an integer value. If the parameter is empty, gParamValue becomes 0.
  for (idx = 0; gCommandTable[idx].name != NULL; idx++) {  // Search for the command in the command table until it is found or the end of the table is reached. If the command is found, break out of the loop.
    if (strcmp(gCommandTable[idx].name, gCommandBuffer) == 0) {
      bCommandFound = true;
      break;
    }
  }

  if (bCommandFound == true) {  // Если команда найдена (в массиве команд), то выполняем ее. Если нет - игнорируем
    (*gCommandTable[idx].function)(server);
  }
  else { // Command not found
    server.print("ERROR: Command not found");
  }
}





void get_JSON_HTML_SD_Static(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  if (type == WebServer::POST)
  {
    server.httpFail();
    return;
  }

  //server.httpSuccess(false, "application/json");
  server.httpSuccess("application/json");


  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */

  if (type == WebServer::HEAD)
    return;




  Streamprint(server,"{"); 
  Streamprint(server,"\"ip\":\"");
  server.print(Ethernet.localIP());
  Streamprint(server,"\",");
  Streamprint(server,"\"ramSize\":\"");
  server.print(sys.ramSize());   
  Streamprint(server,"\",");
  Streamprint(server,"\"Version\":\"");    
  server.print(VERSION_STRING);
  Streamprint(server,"\",");
  Streamprint(server,"\"Data\":\"");                 
  server.print(COMPILE_DATE_STRING);  
  Streamprint(server,"\",");
  //---------------------

  Streamprint(server,"\"SD_Type\":\"");
  switch(card.type()) {
  case SD_CARD_TYPE_SD1:
    server.print("SD1");
    break;
  case SD_CARD_TYPE_SD2:
    server.print("SD2");
    break;
  case SD_CARD_TYPE_SDHC:
    server.print("SDHC");
    break;
  default:
    Streamprint(server,"-");
  } 
  uint32_t volumesize;
  Streamprint(server,"\",");
  Streamprint(server,"\"FAT\":\"");
//  Serial.print(volume.fatType(), DEC); 
  Streamprint(server,"\",");
  Streamprint(server,"\"VolumeSD\":\"");  
  volumesize = volume.blocksPerCluster();    // clusters are collections of blocks
  volumesize *= volume.clusterCount();       // we'll have a lot of clusters
  volumesize *= 512;                            // SD card blocks are always 512 bytes
  volumesize /= 1048576;
//  Serial.print(volumesize);
  Streamprint(server,"\"");
  //---------------------- 
  Streamprint(server,"}"); 

}



void Reboot()
{
  noInterrupts();
  __asm("wdr");
  WDTCSR = (1<<WDCE) | (1<<WDE);
  WDTCSR = (1<<WDE);
  for(;;){};
}


