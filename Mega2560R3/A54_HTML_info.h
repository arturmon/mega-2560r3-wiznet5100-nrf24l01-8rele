#ifndef __A54_HTML_INFO
#define __A54_HTML_INFO

extern void infoHTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void get_info_JSON_HTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void get_JSON_HTML_SD_Static(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);

#endif
