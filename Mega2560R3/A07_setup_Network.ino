/**
* setupNetwork() function
* This function is used to setupup the network according to the values stored in the eeprom
*
* Overview:

* - Display a link to the ethernet setup
* - Check if DHCP should be used, if not create instaces of IPAddress for ip, gateway, subnet and dns_server
* - Invoke Ethernet.begin with all parameters if no dhcp is active (Ethernet.begin(mac, ip, dns_server, gateway, subnet);). 
* - If DHCP is used invoke only with mac (Ethernet.begin(mac);) and display the ip on the serial console.
*/




// Тоже нужно перетащить в ini файл

void printErrorMessage(uint8_t e, bool eol = true)
{
  switch (e) {
  case IniFile::errorNoError:
    Serial.print("no error");
    break;
  case IniFile::errorFileNotFound:
    Serial.print("file not found");
    break;
  case IniFile::errorFileNotOpen:
    Serial.print("file not open");
    break;
  case IniFile::errorBufferTooSmall:
    Serial.print("buffer too small");
    break;
  case IniFile::errorSeekError:
    Serial.print("seek error");
    break;
  case IniFile::errorSectionNotFound:
    Serial.print("section not found");
    break;
  case IniFile::errorKeyNotFound:
    Serial.print("key not found");
    break;
  case IniFile::errorEndOfFile:
    Serial.print("end of file");
    break;
  case IniFile::errorUnknownError:
    Serial.print("unknown error");
    break;
  default:
    Serial.print("unknown error value");
    break;
  }
  if (eol)
    Serial.println();
}

void setupNetwork() {


   const char *filename = "/net.ini";
   
   const size_t bufferLen = 80;
   char bu[bufferLen];
    
   IniFile ini(filename);
   
   if (!ini.open()){
     
     Serial.print("Network ini file ");
     Serial.print(filename);
     Serial.println(" does not exist");
     
     while(1); // !!!!!!!Тут нужно что-то по умолчания сделать 
     
   }
   
   Serial.println("Network ini file exist");
   
   if (!ini.validate(bu, bufferLen)){
     Serial.print("Network ini file ");
     Serial.print(ini.getFilename());
     Serial.println(" not valid");
     
     // Добавить вывод ошибки

     while(1); // !!!!!!!Тут нужно что-то по умолчания сделать 
   }
   
   
  
    IPAddress ip;
 //   IPAddress ip_ntp;
    IPAddress rserver;
    IPAddress gateway;                      
    IPAddress subnet;  
    IPAddress dns_server;
    
    uint8_t mac[6];
    uint16_t wport;

    // Пока добавим без проверки

    if(ini.getMACAddress("network", "mac", bu, bufferLen, mac)){

    } else
    {
      printErrorMessage(ini.getError());
    }
    ini.getIPAddress("network", "ip", bu, bufferLen, ip);
    ini.getIPAddress("network", "ip_ntp", bu, bufferLen, ip_ntp);  
    ini.getValue("network", "ntp_zone", bu, bufferLen, timeZone);      
    ini.getIPAddress("network", "rserver", bu, bufferLen, rserver);     
    ini.getIPAddress("network", "dns", bu, bufferLen, dns_server);
    ini.getIPAddress("network", "gateway", bu, bufferLen, gateway);
    ini.getIPAddress("network", "subnet", bu, bufferLen, subnet);
    ini.getValue("network", "port", bu, bufferLen, wport);
    
  

    Ethernet.begin(mac, ip/*, dns_server, gateway, subnet*/);
    
    webserver = new WebServer(PREFIX, wport);    
    Telnetserver = new WebServer(PREFIX,23); // Telnet listens on port 23
 
 
    Serial.println(Ethernet.localIP());
    
     IPAddress timeServer(ip_ntp);
   Serial.println(timeServer);
   
   

}


