#include "Arduino.h"

void get1wireDevices(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  if (type == WebServer::POST)
  {
    server.httpFail();
    return;
  }

  //server.httpSuccess(false, "application/json");
  server.httpSuccess("application/json");


  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */

  if (type == WebServer::HEAD)
    return;

  //  sensors.requestTemperatures(); // Send the command to get temperatures

  //TODO получить все устройства на шине и выдать на страницу
  int numberOfDevices = sensors.getDeviceCount();
  sensors.begin();

  // server.println("{\"DS\":[");  

  for(int i=0;i<numberOfDevices; i++) {
    if(sensors.getAddress(Termometers, i))
    {


      server.print("{\"id\":\"");
      for (uint8_t i = 0; i < 8; i++) {
        if (Termometers[i] < 16) server.print("0");
        server.print(Termometers[i], HEX);
      }
      float tempC = sensors.getTempC(Termometers);

      //    server.print("\",\"name\":");
      //    server.print(Sensor[i]);//имя датчика температуры
      server.print("\",\"val\":\"");
      server.print(tempC);
      server.print("\"}");
      if(i==numberOfDevices) server.print(",");
    } 
    else {
      // not found
      server.print("NOT FOUND");
    }
  }
  //   server.print("]}");

}




