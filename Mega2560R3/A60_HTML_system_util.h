#ifndef __A60_HTML_SYSTEM_UTIL
#define __A60_HTML_SYSTEM_UTIL

extern void ErrorMessage(WebServer &server);
extern void parsedRequest(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void stateRequest(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void errorHTML(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void cliProcessCommand(WebServer &server);
extern void get_JSON_HTML_SD_Static(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);


#endif
