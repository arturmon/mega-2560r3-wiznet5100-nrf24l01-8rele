#include "Arduino.h"

/* Пример использования

класс, который проверяет клики, даблклики и удержания кнопок. Подразумевается, что в свободном состоянии на входе 1,
в нажатом состоянии 0, что в общем довольно стандартно с учётом встроенных подтягивающих резисторов.


Button b_next(next_button);
Button b_prev(prev_button);
byte current_alg = 2;

void loop() {
	b_next.process_button();
	b_prev.process_button();
	if (b_next.is_clicked()) {
		++current_alg;
		shift_out(latchpin, clockpin, datapin, 0);
	}
	if (b_prev.is_clicked()) {
		--current_alg;
		shift_out(latchpin, clockpin, datapin, 0);
	}
	if (b_prev.is_dblclicked()) {
		current_alg = random(8);
		shift_out(latchpin, clockpin, datapin, 0);
	}
	if (b_next.is_dblclicked()) {
		current_alg = random(8);
		shift_out(latchpin, clockpin, datapin, 0);
	}
	current_alg &= 7;
// ....... последующий код.....
}
*/


class Button {
	byte pin, prev_state;
	bool _clicked, _held, _dblclicked;
	unsigned long pressed_time, uct, clct;
	static const unsigned int jtime = 10;
	static const unsigned int htime = 100;
	static const unsigned int dbltime = 300;
public:
	void process_button() {
		byte val = digitalReadFast(pin);
		_clicked = false;
		_dblclicked = false;
		if (val == 0 && prev_state == 1) {
			uct = 0;
			unsigned long now = millis();
			if (pressed_time == 0)
				pressed_time = now;
			if (now > pressed_time + jtime) {
				prev_state = val;
				_clicked = true;
				if (clct != 0 && now < clct + dbltime) {
					_dblclicked = true;
				}
				clct = pressed_time;
			}
		} else if (val == 1 && prev_state == 0) {
			pressed_time = 0;
			if (uct == 0)
				uct = millis();
			if (millis() > uct + jtime) {
				prev_state = val;
				_held = false;
				uct = millis();
			}
		}
		if (val == 0 && prev_state == 0 && millis() > clct + htime) {
			_held = true;
		} else {
			_held = false;
		}
	}
	Button(byte _pin) :
			pin(_pin) {
		prev_state = 1;
		_dblclicked = _held = _clicked = false;
		clct = uct = pressed_time = 0;
	}

	bool is_clicked() {
		return _clicked;
	}
	unsigned long held_time() {
		if (_held)
			return millis() - clct;
		return 0;
	}
	bool is_dblclicked() {
		return _dblclicked;
	}
};

