/**********************************************************************************************************************
* Поиск устройств (датчиков температуры на шине 1-wire)
**/
void searchDevices() {
   //Serial.print("Start search on 1-wire");
   Serialprint("Start search on 1-wire");

   int numberOfDevices = sensors.getDeviceCount();
   sensors.begin();
   
   for(int i=0;i<numberOfDevices; i++) {
      if(sensors.getAddress(Termometers, i))
      {
          Serialprint("Found device ");
	  Serial.print(i, DEC);
          Serialprint(" with address: ");
          for (uint8_t i = 0; i < 8; i++) {
            if (Termometers[i] < 16) Serialprint("0");
              Serial.print(Termometers[i], HEX);
          }

          Serialprint(" Resolution actually set to: ");
	  Serial.print(sensors.getResolution(Termometers), DEC); 
          Serial.println();
          float tempC = sensors.getTempC(Termometers);
          Serial.print(tempC);
          //Serial.println("C");
          Serialprint("C\n");
      
      } else {
            // not found
      }
    }

}
