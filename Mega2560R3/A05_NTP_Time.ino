#include "Arduino.h"
/*
void makeISOdate(){
  sprintf(isodate, "%4d-%02d-%02dT%02d:%02d:%02d%+05d\n",
      year(), month(), day(), hour(), minute(), second(), timeZoneOffset/36 );
}
*/
void digitalClockDisplay(){
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Streamprint(Serial," ");
  Serial.print(day());
  Streamprint(Serial," ");
  Serial.print(month());
  Streamprint(Serial," ");
  Serial.print(year()); 
  Streamprint(Serial,"\n\r"); 
}

void printDigits(int digits){
  // utility for digital clock display: prints preceding colon and leading 0
  Streamprint(Serial,":");
  if(digits < 10)
    Streamprint(Serial,"0");
  Streamprint(Serial,"%d",digits);
}

void update_Time_NTP(){
  IPAddress timeServer(ip_ntp);
    lcd.setCursor(0,1);
  Serialprint("waiting for sync ");
  Serial.println(timeServer);
  Serialprint("\n\r");
  Streamprint(lcd,"waiting for sync");
  setSyncProvider(getNtpTime);
}


time_t getNtpTime()
{
    IPAddress timeServer(ip_ntp);
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Streamprint(Serial,"Transmit NTP Request\n\r");
  sendNTPpacket(timeServer);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Streamprint(Serial,"Receive NTP Response\n\r");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
    }
  }
  Streamprint(Serial,"No NTP Response :-(\n\r");
  return 0; // return 0 if unable to get the time
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress &address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:                 
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

