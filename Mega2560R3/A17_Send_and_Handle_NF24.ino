#include "Arduino.h"


/**
 * Send a 'M' message, the current time
 * Отправить сообщение "M", текущее время
 */
bool send_M(uint16_t to)
{
/*
  payload_t payload = { 
    type, number, float(value)          };
*/
  RF24NetworkHeader header(/*to node*/ to, /*type*/ 'M' /*Time*/);

  // The 'T' message that we send is just a ulong, containing the time
  // «Т» сообщение, что мы посылаем это просто ulong, содержащий time
//  unsigned long message = millis();
  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP Sending to 0%o... Value- %d\n\r"),millis(),to,F_tt.value);
  return network.write(header,&F_tt,sizeof(F_tt));
}

/**
 * Send an 'N' message, the active node list
 * Отправить сообщение 'N', активный список узлов
 */
bool send_N(uint16_t to)
{
  RF24NetworkHeader header(/*to node*/ to, /*type*/ 'N' /*Time*/);

  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP Sending active nodes to 0%o...\n\r"),millis(),to);
  return network.write(header,active_nodes,sizeof(active_nodes));
}




/**
 * Handle a 'M' message
 *
 * Add the node to the list of active nodes
 * Добавить узел в список активных узлов
 */
void handle_M(RF24NetworkHeader& header)
{
    payload_t payload;
    network.read(header,&payload,sizeof(payload));
  printf_P(PSTR("---------------------------------\n\r"));
  printf_P(PSTR("%lu: APP Received nodes 0%o type ID %d number %d value %d\n\r"),millis(),header.from_node,payload.type,payload.number,payload.value);
  if ( header.from_node != this_node || header.from_node > 00 )
//    add_node(header.from_node);
 check_Value_is_Node(header.from_node, payload.number,payload.type,payload.value);

}

/**
 * Handle an 'N' message, the active node list
 * Обращение сообщение 'N', активный список узлов
 */
void handle_N(RF24NetworkHeader& header)
{
  static uint16_t incoming_nodes[max_active_nodes];

  network.read(header,&incoming_nodes,sizeof(incoming_nodes));
  printf_P(PSTR("%lu: APP Received nodes from 0%o\n\r"),millis(),header.from_node);

  int i = 0;
  while ( i < max_active_nodes && incoming_nodes[i] > 00 )
    add_node(incoming_nodes[i++]);
}



/**
 * Add a particular node to the current list of active nodes
 * Добавить конкретный узел в текущий список активных узлов
 */
void add_node(uint16_t node)
{
  // Do we already know about this node?
  // Разве мы уже знаем об этой узла?
  short i = num_active_nodes;
  while (i--)
  {
    if ( active_nodes[i] == node )
      break;
  }
  // If not, add it to the table
  // Если нет, добавьте его к столу
  if ( i == -1 && num_active_nodes < max_active_nodes )
  {
    active_nodes[num_active_nodes++] = node; 
    printf_P(PSTR("%lu: APP Added 0%o to list of active nodes.\n\r"),millis(),node);
  }
}








