#include "Arduino.h"


void get_info_JSON_HTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  if (type == WebServer::POST)
  {
    server.httpFail();
    return;
  }

  //server.httpSuccess(false, "application/json");
  server.httpSuccess("application/json");


  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */

  if (type == WebServer::HEAD)
    return;
  Streamprint(server,"{"); 
  Streamprint(server,"\"uptime\":\"");  
  server.print(sys.uptime());
  Streamprint(server,"\",");
  Streamprint(server,"\"ramFree\":\"");
  server.print(sys.ramFree());
  Streamprint(server,"\",");
  Streamprint(server,"\"devTemp\":\"");              
  server.print(Device_temp_HTML);
  Streamprint(server,"\"");
  Streamprint(server,"}"); 
}











