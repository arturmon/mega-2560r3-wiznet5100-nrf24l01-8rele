#include "Arduino.h"



void get_net_JSON_HTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete)
{
  if (type == WebServer::POST)
  {
    server.httpFail();
    return;
  }

  server.httpSuccess("application/json");


  /* if we're handling a GET or POST, we can output our data here.
   For a HEAD request, we just stop after outputting headers. */

  if (type == WebServer::HEAD)
    return;


  const char *filename = "/net.ini";

  const size_t bufferLen = 80;
  char bu[bufferLen];

  IniFile ini(filename);

  if (!ini.open()){

    Serial.print("Network ini file ");
    Serial.print(filename);
    Serial.println(" does not exist");

    while(1); // !!!!!!!Тут нужно что-то по умолчания сделать 

  }

  Serial.println("Network ini file exist");

  if (!ini.validate(bu, bufferLen)){
    Serial.print("Network ini file ");
    Serial.print(ini.getFilename());
    Serial.println(" not valid");

    // Добавить вывод ошибки

    while(1); // !!!!!!!Тут нужно что-то по умолчания сделать 
  }
  
    IPAddress ip;
    IPAddress rserver;
    IPAddress ip_ntp;
    IPAddress gateway;                      
    IPAddress subnet;  
    IPAddress dns_server;
    uint8_t mac[6];
    uint16_t wport;
    uint16_t dhcp;
  
  ini.getMACAddress("network", "mac", bu, bufferLen, mac);
  ini.getIPAddress("network", "ip", bu, bufferLen, ip);
  ini.getIPAddress("network", "ip_ntp", bu, bufferLen, ip_ntp);
  ini.getValue("network", "ntp_zone", bu, bufferLen, timeZone);  
  ini.getIPAddress("network", "rserver", bu, bufferLen, rserver);
  ini.getIPAddress("network", "dns", bu, bufferLen, dns_server);
  ini.getIPAddress("network", "gateway", bu, bufferLen, gateway);
  ini.getIPAddress("network", "subnet", bu, bufferLen, subnet);
  ini.getValue("network", "port", bu, bufferLen, wport); 
  ini.getValue("network", "dhcp", bu, bufferLen, dhcp);
 
 
  //----NTP_sync---------
  Serialprint("waiting for sync ");
    IPAddress timeServer(ip_ntp);
  Serial.println(timeServer);
  update_Time_NTP();
  //----NTP_sync---------

  Streamprint(server,"{"); 

  for (int a=0;a<6;a++) {
    Streamprint(server,"\"Mac"); 
    Streamprint(server,"%d",a);
    Streamprint(server,"\":\"");
    server.print(mac[a],HEX);
    Streamprint(server,"\",");  
  }
    
    
  for (int a=0;a<4;a++) {
    Streamprint(server,"\"Ip");
    Streamprint(server,"%d",a+6);
    Streamprint(server,"\":\"");
    server.print(ip[a]);
    Streamprint(server,"\",");
  }


  for (int a=0;a<4;a++) {
    Streamprint(server,"\"Ipr");
    Streamprint(server,"%d",a+10);
    Streamprint(server,"\":\"");
    server.print(rserver[a]);
    Streamprint(server,"\",");
  }

  for (int a=0;a<4;a++) {
    Streamprint(server,"\"IpNTP");
    Streamprint(server,"%d",a+29);
    Streamprint(server,"\":\"");
    server.print(ip_ntp[a]);
    Streamprint(server,"\",");
  }

  Streamprint(server,"\"Zone33\":\"");
   server.print(timeZone);

  Streamprint(server,"\",");

  for (int a=0;a<4;a++) {
    Streamprint(server,"\"SUb");
    Streamprint(server,"%d",a+14);
    Streamprint(server,"\":\"");
    server.print(subnet[a]);
    Streamprint(server,"\",");
  }



  for (int a=0;a<4;a++) {
    Streamprint(server,"\"Gw");
    Streamprint(server,"%d",a+18);
    Streamprint(server,"\":\"");
    server.print(gateway[a]);
    Streamprint(server,"\",");
  }



  for (int a=0;a<4;a++) {
    Streamprint(server,"\"Dns");
    Streamprint(server,"%d",a+24);
    Streamprint(server,"\":\"");
    server.print(dns_server[a]);
    Streamprint(server,"\",");
  }




  Streamprint(server,"\"WEBPort\":\"");
   server.print(wport);

  Streamprint(server,"\",");
  Streamprint(server,"\"USEdhcp\":\"");
  /*
  if(eeprom_config.use_dhcp != 1) {
   Streamprint(server,">Off"); 
   }
   else if(eeprom_config.use_dhcp == 1) {
   Streamprint(server,">On"); 
   }
   */
  if(dhcp != 1) {
    Streamprint(server,"0"); 
   //Streamprint(server,">Off"); 
   }
   else if(dhcp == 1) {
     Streamprint(server,"1"); 
   //Streamprint(server,">On"); 
   }   
   
  Streamprint(server,"\"");
  Streamprint(server,"}"); 
}




