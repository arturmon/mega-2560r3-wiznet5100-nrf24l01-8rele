#ifndef __A51_HTML_SETUP_PIN
#define __A51_HTML_SETUP_PIN

extern void setupPinHTML(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);

#endif
