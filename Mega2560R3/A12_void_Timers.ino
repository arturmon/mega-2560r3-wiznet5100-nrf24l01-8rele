void elapsed1()
{

  // Serialprint("-----------------TIMER-1(tmr1)-----------------\n\r");  

  for (int i=1; i < Number_of_Sensors; i++){
    Read_State_Sensors (00, i);
  } 
  check_RF24_Network();

}

void elapsed2()
{


  // Serialprint("-----------------TIMER-2(tmr2)-----------------\n\r");  
  if (lst.pHead == NULL){
    for (int i=0; i < Number_of_Sensors; i++){
      check_Value_is_Node(00, i+1,Sensor_ID[i],0);
      check_Input_Output_Sensors (i+1); //стартовые настройки портов на меге, так же можно вызывать после изменения типа входа (работает только для меги)
    }
    for (int i=1; i < Number_of_Sensors; i++){
      Read_State_Sensors (00, i); //чтение состояния входа (применимо и для беспроводных клиентов )
    }
  }

  Print_LCD(Type_Screen_LCD);
  //--------------------------GSM-------------------------------------
#ifdef USED_GSM
  if(started){
    //Read if there are messages on SIM card and print them.
    if(gsm.readSMS(smsbuffer, 160, n, 20))
    {
      Serial.println(n);
      Serial.println(smsbuffer);
    }
  }
#endif
  //--------------------------GSM-------------------------------------
}

void elapsed3()
{


  Serialprint("-----------------TIMER-3(tmr3)-----------------\n\r");  
  //send_MDM();
  // printCardInfo();
  sensors.requestTemperatures(); // Обновление показаний датчиков температуры DS
  if(sensors.getAddress(Termometers, 0))
  {
    Device_temp_HTML=sensors.getTempCByIndex(0);

  } 
  else {
    // not found
    Device_temp_HTML=0;
  }

}

void elapsed4()
{


  Serialprint("-----------------TIMER-4(tmr4)-----------------\n\r");  
  lst.Sort();


}

void elapsed5()
{


  Serialprint("-----------------TIMER-5(tmr5)-----------------\n\r");  
  //----NTP_sync---------
  Serialprint("waiting for sync ");
    IPAddress timeServer(ip_ntp);
  Serial.println(timeServer);
  update_Time_NTP();
  //----NTP_sync---------
}








