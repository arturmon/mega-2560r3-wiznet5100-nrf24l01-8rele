#ifndef __A52_HTML_SETUP_RELE
#define __A52_HTML_SETUP_RELE

extern void setupReleHTML(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void setupReleHTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);


#endif
