#ifndef __A53_HTML_SETUP_NET
#define __A53_HTML_SETUP_NET

extern void Net_Setup_HTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);
extern void get_net_JSON_HTML_SD(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);

#endif
