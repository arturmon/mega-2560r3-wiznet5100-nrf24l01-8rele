/* 
	Editor: http://www.visualmicro.com
	        arduino debugger, visual micro +, free forum and wiki
	
	Hardware: Arduino Mega 2560 or Mega ADK, Platform=avr, Package=arduino
*/

#define __AVR_ATmega2560__
#define ARDUINO 101
#define ARDUINO_MAIN
#define F_CPU 16000000L
#define __AVR__
#define __cplusplus
extern "C" void __cxa_pure_virtual() {;}

void set_EEPROM_Default();
//
//
void searchDevices();
void read_EEPROM_Settings();
void setupNetwork();
void Print_LCD(uint8_t Type_Screen);
boolean verify_initialization_SD();
bool send_MDM(String classes,unsigned int variable,float value);
int Counter(int Pin_count);
void elapsed1();
void elapsed2();
void elapsed3();
void elapsed4();
void elapsed5();
void Check_Telnet_Connect();
void printPrompt();
void checkConnectionTimeout();
void getReceivedText();
void parseReceivedText();
void doDigitalCommand();
void readDigitalPins();
void outputPinState(int pin);
void writeDigitalPin();
int parsePinSetting();
void doAnalogCommand();
void readAnalogPins();
void writeAnalogPin();
void setPinMode();
int parseModeSetting();
void doTimersCommand();
void readTimers();
void writeTimers();
long parseSetting(int textPosition, int endTextPosition);
void    writeStart_Stop_Timers();
void doStructCommand();
void list_Struct_Telnet(void);
void Dell_ALL_Struct_Telnet(void);
void Sort_ALL_Struct_Telnet(void);
void printInfoDeviceMessage();
int parseDigit(char c);
void printErrorMessage(int error_number);
void checkCloseConnection();
void closeConnection();
void printHelpMessage();
void check_RF24_Network();
void Check_Send_Message(unsigned int to, byte type, byte number, int value);
bool send_M(uint16_t to);
bool send_N(uint16_t to);
void handle_M(RF24NetworkHeader& header);
void handle_N(RF24NetworkHeader& header);
void add_node(uint16_t node);
void check_Value_is_Node(unsigned int from_node, byte number, byte type,int value);
float check_send_value (int value, int type);
void check_Input_Output_Sensors (byte Number_of_Sensors_Temp);
unsigned int Read_State_Sensors (unsigned int from_node, byte Number_of_Sensors_Temp);
unsigned int Write_State_Sensors (unsigned int from_node, byte Number_of_Sensors_Temp, int Sensor_value_temp);
int ConvertTo(int number , int radix );
void ErrorMessage(WebServer &server);
void cliProcessCommand(WebServer &server);
void renewDHCP(int interval);
void commandsDht(WebServer &server);
void commandsSwitch(WebServer &server);
void commands_Read_Sensor(WebServer &server);
void commandsAn(WebServer &server);
void commandsDig(WebServer &server);
void commandsOn(WebServer &server);
void commandsOff(WebServer &server);
void commandsClick(WebServer &server);
void commandsLClick(WebServer &server);
void commandsStatus(WebServer &server);
void commandsHelp(WebServer &server);
void initialDHT();

#include "D:\util\arduino-1.5.2\hardware\arduino\avr\variants\mega\pins_arduino.h" 
#include "D:\util\arduino-1.5.2\hardware\arduino\avr\cores\arduino\arduino.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\Mega2560R3.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A01_Setup.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A02_void_loop.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A03_void_searchDevice.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A04_void_Read_PIR.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A06_read_EEPROM_Settings.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A07_setup_Network.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A08_Print_LCD.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A09_setup_SD.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A09_setup_SD.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A100_HTML_index.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A100_HTML_index.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A10_void_send_MDM.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A11_void_Counter.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A12_void_Timers.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A13_Telnet_Server.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A15_Utils_function.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A15_Utils_function.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A16_Check_RF24.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A17_Send_and_Handle_NF24.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A17_Send_and_Handle_NF24.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A20_Logic_of_the_Structure.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A20_Logic_of_the_Structure.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A25_Class_Buttons.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A50_HTML_setup_WIFI.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A50_HTML_setup_WIFI.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A51_HTML_setup_Pin.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A51_HTML_setup_Pin.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A52_HTML_setup_Rele.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A52_HTML_setup_Rele.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A53_HTML_setup_NET.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A53_HTML_setup_NET.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A54_HTML_info.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A54_HTML_info.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A55_HTML_get_1wire_device.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A55_HTML_get_1wire_device.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A60_HTML_system_util.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A60_HTML_system_util.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A80_HTML_Command_execution.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\A80_HTML_Command_execution.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\EEPROMAnything.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\System.cpp"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\System.h"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\initialDHT.ino"
#include "C:\Users\artur_000\Desktop\local_arduino\Arturmon\Mega\mega_temp_v3\Mega2560R3\printf.h"
