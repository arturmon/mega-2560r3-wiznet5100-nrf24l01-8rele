#ifndef __A50_HTML_SETUP_WIFI
#define __A50_HTML_SETUP_WIFI

extern void setupWifiHTML(WebServer &server, WebServer::ConnectionType type, char *url_tail, bool tail_complete);

#endif
