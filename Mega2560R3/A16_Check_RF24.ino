#include "Arduino.h"

void check_RF24_Network(){
  uint16_t from;
  // Pump the network regularly
  // 
  network.update();
  while ( network.available() )
  {

    // If so, take a look at it 
    // Если это так, посмотрите на него
    RF24NetworkHeader header;
    network.peek(header);
    printf_P(PSTR("%lu: APP Received #%u type %c from 0%o\n\r"),millis(),header.id,header.type,header.from_node);

    // Dispatch the message to the correct handler.
    // Отправить сообщение на правильный обработчик.
    switch (header.type)
    {
    case 'M':
      handle_M(header);
      break;
    case 'N':
      handle_N(header);
      break;
    default:
      printf_P(PSTR("*** WARNING *** Unknown message type %c\n\r"),header.type);
      network.read(header,0,0);
      break;
    };


  } 


}



void Check_Send_Message(unsigned int to, byte type, byte number, int value)
{
  F_tt.value = value;
  F_tt.number = number;
  F_tt.type = type;
  bool ok;
Serial.println( oct2dec(to));
  ok = send_M( oct2dec(to));

  if (ok)
  {
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Sending ok to Node %d\n\r"),millis(),to);
#endif
    check_Value_is_Node(oct2dec(to),number,type,value); //если пакет отправлен обновляем общий список
  }
  else
  {
#ifdef DEDUG
    printf_P(PSTR("%lu: APP Sending failed to Node %d\n\r"),millis(),to);
#endif    
  }
}







