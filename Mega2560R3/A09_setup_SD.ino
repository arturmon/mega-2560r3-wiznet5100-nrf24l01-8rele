#include "Arduino.h"


boolean verify_initialization_SD(){
  SWITCH_TO_SD
    boolean done = false;
    delay(10);
  if (!sd.begin(selectSd, SPI_FULL_SPEED)){
    //if (!SD.begin(selectSd)) {
      lcd.clear();
      lcd.setCursor(2,0);
      Streamprint(lcd,"---ERROR---");
      lcd.setCursor(0,1);
      Streamprint(lcd,"Cant`t access SD");
    sd.initErrorHalt();
    Serialprint("ERROR - SD card initialization failed. Things to check:\n");
    Serialprint("* is a card is inserted?\n");
    Serialprint("* Is your wiring correct?\n");
    Serialprint("* did you change the chipSelect pin to match your shield or module?\n");
    return done;
  } 
  else {
    Serialprint("SUCCESS - SD card initialized.\n");
    done = true; 
  }
  return done;
}




P(CT_PNG) = "image/png\n";
P(CT_JPG) = "image/jpeg\n";
P(CT_HTML) = "text/html\n";
P(CT_CSS) = "text/css\n";
P(CT_PLAIN) = "text/plain\n";
P(CT_WOFF) = "application/font-woff\n";
P(CT_SVG) = "aimage/svg+xml\n";
P(CT_TTF) = "application/x-font-ttf\n";
P(CT) = "Content-type: ";
P(OK) = "HTTP/1.0 200 OK\n";
void fetchSD(WebServer &server, WebServer::ConnectionType type, char *urltail, bool){
  //void fetchSD(WebServer &server, WebServer::ConnectionType type, char *urltail, bool tailcomplete){


  byte buff[BUFFER_SIZE]; 
  char buf[32];
  int16_t  readed;

  ++urltail;
  char *dot_index; //Where dot is located

  if (strstr(urltail, "Net.htm?0=")){
    Streamprint(Serial,"xo xo Net.htm");
    bool ok;
    //******************************************************************************************************
    URLPARAM_RESULT rc;
    char name[NAMELEN];
    char value[VALUELEN];
    boolean params_present = false;
    byte param_number = 0;

    /* this line sends the standard "we're all OK" headers back to the
     browser */
    server.httpSuccess();

    /* if we're handling a GET or POST, we can output our data here.
     For a HEAD request, we just stop after outputting headers. */
    if (type == WebServer::HEAD)
      return;


  SdFile wrfile("net.ini", O_WRITE | O_CREAT | O_TRUNC);
  
  // check for open error
  if (!wrfile.isOpen()) sd.errorHalt_P(PSTR("MakeTestFile"));
 wrfile.print("[network]\r\n"); 
  
    // check for parameters
    if (strlen(urltail)) {
      while (strlen(urltail)) {
        rc = server.nextURLparam(&urltail, name, NAMELEN, value, VALUELEN);
        if (rc != URLPARAM_EOS) {
          params_present=true;

          param_number = atoi(name);
          
          if ( param_number == 0 )wrfile.print("mac = ");
          if ( param_number == 6 )wrfile.print("ip = ");
          if ( param_number == 29 )wrfile.print("ip_ntp = "); 
           if ( param_number == 33 )wrfile.print("ntp_zone = ");         
          if ( param_number == 10 )wrfile.print("rserver = ");  
          if ( param_number == 14 )wrfile.print("subnet = "); 
           if ( param_number == 18 )wrfile.print("gateway = ");
          if ( param_number == 24 )wrfile.print("dns = ");
          if ( param_number == 28 )wrfile.print("port = ");
          if ( param_number == 23 )wrfile.print("dhcp = ");          
 
          
          // read MAC address
          if (param_number >=0 && param_number <=5) {
                 wrfile.print(value);
                 if (param_number != 5) wrfile.print(":");
          }

          // read IP address
          if (param_number >=6 && param_number <=9) {
                 wrfile.print(atoi(value));
                 if (param_number != 9) wrfile.print(".");
          }

          // read IP address RSERVER
          if (param_number >=10 && param_number <=13) {
                 wrfile.print(atoi(value));
                 if (param_number != 13) wrfile.print(".");      
          }
          // read IP address NTP
          if (param_number >=29 && param_number <=32) {
                 wrfile.print(atoi(value));
                 if (param_number != 32) wrfile.print(".");      
          }          

          // read NTP Time Zone port
          if (param_number == 33) {
                 wrfile.print(atoi(value));
          }

          // read SUBNET
          if (param_number >=14 && param_number <=17) {
                 wrfile.print(atoi(value));
                 if (param_number != 17) wrfile.print(".");     
          }

          // read GATEWAY
          if (param_number >=18 && param_number <=21) {
                 wrfile.print(atoi(value));
                 if (param_number != 21) wrfile.print(".");        
          }

          // read DNS-SERVER
          if (param_number >=24 && param_number <=27) {
                 wrfile.print(atoi(value));
                 if (param_number != 27) wrfile.print(".");      
          }

          // read WEBServer port
          if (param_number == 28) {
                 wrfile.print(atoi(value));
          }

          // read DHCP ON/OFF
          if (param_number == 23) {
                 wrfile.print(atoi(value));
          }
          if ( param_number == 5 || param_number == 9 || param_number == 13 || param_number == 32 || param_number == 33 || param_number == 17 || param_number == 21 || param_number == 27 || param_number == 28 || param_number == 23 )wrfile.print("\r\n");          
        }
      }
    }

  wrfile.close();

    Read_File_Net();


  if (!myFile.open("Net.htm", O_READ)) {
    sd.errorHalt("opening Net.htm failed");
  }
    while(myFile.available()) {
      int n = myFile.read((char*)buff, BUFFER_SIZE);
      if (!n)
        break;
      server.write(buff, n);
    }

    myFile.close(); 
  //  Reboot();

return;
    //******************************************************************************************************

  }


  if (!myFile.open( urltail, O_READ)) {
    //Real 404
    server.httpSuccess();
    Streamprint(server,"ERROR NOT FOUND 404 ");
    Streamprint(Serial,"ERROR NOT FOUND 404 ");   
    Serial.println (urltail);   

  } 
  else {
    if (dot_index = strstr(urltail, ".")) {
      ++dot_index;
      server.printP(OK);
      server.printP(CT);
      if (!strcmp(dot_index, "htm")) {
        server.printP(CT_HTML);

      } 
      else if (!strcmp(dot_index, "css")) {
        server.printP(CT_CSS);

      } 
      else if (!strcmp(dot_index, "jpg")) {
        server.printP(CT_JPG);

      }
      else if (!strcmp(dot_index, "wof")) {//woff
        server.printP(CT_WOFF);

      }
      else if (!strcmp(dot_index, "svg")) {
        server.printP(CT_SVG);

      }  
      else if (!strcmp(dot_index, "ttf")) {
        server.printP(CT_TTF);

      }      
      else {
        server.printP(CT_PLAIN);
      }
      server.print(CRLF);
    }

    /*
	readed = myFile.read(buf,30);
     	while( readed > 0) {
     		buf[readed] = 0;
     		bufferedSend(server,buf,readed);
     		readed = myFile.read(buf,30);
     	}
     	flushBuffer(server);
     	myFile.close();
     */
    Streamprint(Serial,"Load - ");   
    Serial.println (urltail);  

    while(myFile.available()) {
      int n = myFile.read((char*)buff, BUFFER_SIZE);
      if (!n)
        break;
      server.write(buff, n);
    }
    myFile.close(); 



  }
}



void Read_File_Net() {
  char line[25];
  int c;
  uint32_t pos;

  // open test file
  SdFile rdfile("net.ini", O_RDWR);

  // check for open error
  if (!rdfile.isOpen())  sd.errorHalt_P(PSTR("demoFgets"));

  // list file
  Serial.println ("-----Before Rewrite\r\n");
  while ((c = rdfile.read()) >= 0) Serial.write(c); 


  // close so rewrite is not lost
  rdfile.close();
   Serial.println ("-----END------");
}

